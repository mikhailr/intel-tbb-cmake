include_directories(${COMMON_INCLUDE} ${CMAKE_CURRENT_BINARY_DIR} ../rml/include)

get_property(dirs DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR} PROPERTY INCLUDE_DIRECTORIES)
set (string_includes "")
foreach(dir ${dirs})
  set (string_includes "${string_includes} ${dir}")
endforeach()

add_custom_command(
    OUTPUT "${CMAKE_CURRENT_BINARY_DIR}/version_string.ver"
    COMMAND ${PROJECT_SOURCE_DIR}/build/version_info_linux.sh ${CMAKE_CXX_COMPILER} ${CMAKE_CXX_FLAGS} ${string_includes} > version_string.ver
    COMMENT "Regenerating header version_string.ver"
)

#shared
set ( SUBDIR_SHARED_LIBARIES
  tbb
)

set ( tbb_SHARED_LIB_SOURCES
    concurrent_queue
    concurrent_vector
    dynamic_link
    itt_notify
    cache_aligned_allocator
    pipeline
    queuing_mutex
    queuing_rw_mutex
    reader_writer_lock
    spin_rw_mutex
    spin_mutex
    critical_section
    task
    tbb_misc
    tbb_misc_ex
    mutex
    recursive_mutex
    condition_variable
    tbb_thread
    concurrent_monitor
    semaphore
    private_server
    ../rml/client/rml_tbb 
    task_group_context
    governor
    market
    arena
    scheduler
    observer_proxy
    tbb_statistics
    tbb_main
    version_string.ver
)

set ( *_SHARED_LIB_LINKLIBRARIES
  ${COMMON_LIBRARIES}
)

# static
set ( *_SUBDIR_STATIC_LIBARIES
)

set ( *_STATIC_LIB_SOURCES
)


foreach (SHARED_LIBRARY ${SUBDIR_SHARED_LIBARIES})
  add_library(${SHARED_LIBRARY} STATIC ${${SHARED_LIBRARY}_SHARED_LIB_SOURCES})
  target_link_libraries(${SHARED_LIBRARY} ${${SHARED_LIBRARY}_SHARED_LIB_LINKLIBRARIES})
endforeach()




